#!/usr/bin/env bash
# Copyright (c) 2024 Collabora, Ltd.
#
# SPDX-License-Identifier: Apache-2.0
set -e

VENV_DIR=.static_check_venv

rm -rf $VENV_DIR
python3 -m venv $VENV_DIR
. $VENV_DIR/bin/activate

pip install wheel  # always install this first to avoid weirdness with pip

pip install -r requirements.txt
pip install -r dev-requirements.txt

echo "Checking formatting with Black"
black --check .

echo "Checking with Flake8"
flake8 artifact_status

echo "Checking with Pyright"
pyright .

echo "Checking with mypy"
mypy .



deactivate
