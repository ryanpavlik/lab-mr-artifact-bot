#!/usr/bin/env bash
# Copyright (c) 2022 Collabora, Ltd.
#
# SPDX-License-Identifier: Apache-2.0

set -e

. common.sh
echo "Pushing image $TAG"

buildah push "${TAG}"
