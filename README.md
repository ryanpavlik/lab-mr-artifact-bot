# Lab MR Artifact Bot

Maintained by Rylie Pavlik <rylie@ryliepavlik.com> + <rylie.pavlik@collabora.com>
at <https://gitlab.com/ryliepavlik/lab-mr-artifact-bot>.

## Introduction

This is a bundle of functionality allowing either continuous (as webhook)
or on-demand monitoring of merge requests and pipelines for GitLab projects,
automatically submitting a template-based comment.
The initial usage was to provide quick access to artifacts,
but it's not limited to that.
By design, it works with any GitLab installation that is modern enough,
including private ones.

The basic operating principal is the following:
upon web hook or script invokation,
the most recently-completed pipeline associated with one or more merge requests
triggers processing of a configurable template to generate a message/note,
subject to some conditions
(MR open,
pipeline matches latest commit on MR,
MR not already successful and set to merge on success,
a template is found).
The note is then automatically posted on the corresponding merge request,
if an identical note is not already present on that MR.
The web hook bot only touches GitLab upon receiving a web hook,
and only for the corresponding entities,
while the standalone scripts can process all configured projects,
or explicitly enumerated project/merge request pairs.

This makes use of the lovely [gidgetlab](https://gidgetlab.readthedocs.io/en/latest/)
asynchronous GitLab API library for Python,
as well as general features of Python 3.7,
and some other Python packages as well.
(I think it needs 3.7, definitely needs 3.6.)

## Scripts

This project provides several scripts to use its functionality:

- `./run-bot.py` launches a webhook listener HTTP server,
  to comment on merge requests with completed pipelines as they are available.
- `./process-projects.py` is for batch/interactive use:
  running it iterates through all open MRs in the configured projects,
  and attempts to comment on each of them (without posting duplicate comments).
- `./process-mrs.py` is also for batch/interactive use:
  pass one or more project-MR pairs to consider and attempt to comment on,
  with arguments in the format:
  `-m ryliepavlik/lab-mr-artifact-bot!1 -m ryliepavlik/lab-mr-artifact-bot!2`

## Installation

This can be run locally/directly,
deployed on a cloud host (tested on Heroku),
or deployed and/or run interactively using docker.
(The very cool deploy it over docker using GitLab CI :D )

If you are using just the interactive scripts,
you only need to be able to reach your GitLab server
from where you run your script.
If you are running as a webhook bot,
then your GitLab instance must also be able to reach your bot host/port.
For web hook testing, services like [ngrok](https://ngrok.com)
that provide a way to tunnel public web traffic to localhost can be useful.

### Direct execution

If running it directly (not via docker or some brainy cloud host),
you'll want to set up a virtualenv for Python,
install required packages,
then run the bot or script:

```sh
# One time setup: clone
git clone https://gitlab.com/ryliepavlik/lab-mr-artifact-bot.git

# One time setup: create venv
# can provide a full path to python 3.7 if needed
virtualenv venv --python python3.7

# Required every time you want to run an included script.
# Adjust as required for your shell.
source venv/bin/activate

# One time setup: install packages
pip3 install -f requirements.txt
```

If you want to develop the bot's code directly,
it's recommended to install `dev-requirements.txt` as well as `requirements.txt`.

### Docker

Dockerfile and docker-compose configs are provided.
The Dockerfile will copy the source in,
as well as `config.json` *which you must create*.
It's recommended to use docker-compose,
in which case you can create a `docker-compose.override.yml`
file to set environment variables and otherwise configure the bot:
see `docker-compose.yml` for info on how to do this.

Then, these are your steps:

```sh
# Build the image -- required after any config.json or yml change.
docker-compose build

# Run the bot -- add -d if you want it to detach/daemonize (run in background)
docker-compose up

# Once it is running, you may enter the running container and run any of the scripts with
# something like this - change script and/or append arguments as desired.
docker-compose exec web ./process-projects.py
```

### Heroku

This is really only useful for bot deployments,
rather than interactive running of the scripts.
However, it does work last time I tested it.

You'll possibly want to modify `runtime.txt` to the latest Python available from Heroku.
You'll also need to commit a config json file to your fork:
`config.json` is gitignored by default, but you can either force-add it,
or add a file by a different name then specify it in the `BOT_CONFIG_FILES` environment variable.

On Heroku for deployment, you won't use `.env` -
instead you'll add the variables through their CLI or web interface.

You may locally use it, however, for testing.

## Configuration

There are three major parts of configuration: environment, project, and hook.
Project configuration is modified most frequently, so it will be presented first.

### Project configuration

A single bot instance (or config file, for manual execution) can handle
an arbitrary number of projects.
The Jinja2 template used to generate a message is looked for in two places:

- In a file (by default, named `.artifact-bot.md`) in the target branch of the merge request.
- If not found there (or explicitly disabled), a key called `mr_template_source` in the config JSON
  of the project is used.

As alluded to, "non-admin" settings are generally configured in one or more json files,
conventionally called `config.json`.
This project includes a documented sample `sample.config.json`,
as well as a JSON Schema (draft 7) `config.schema.json`
that can be used in editors like
[VS Code](https://code.visualstudio.com/docs/languages/json#_json-schemas-settings).

In general,
any object/dictionary in the config may contain any number of key-value pairs
whose key is or starts with `#` - all such elements are ignored,
and thus this is useful for adding comments.

The top-level object has a key for each project that is monitored.
The key should be the full path to the project
(everything after the URL to your GitLab instance: e.g. `ryliepavlik/lab-mr-artifact-bot`).
The corresponding value is an object that configures the behavior of these tools,
through the following child elements:

- `project_id` - Optional: Numeric project ID for this project, if known.
  Omit if not known, it will be automatically detected upon receiving a webhook.
- `actually_post` - Optional, defaults to `true`, may include and set to `false` to
  simulate operation without actually posting any comments.
- `mr_template_filename` - Optional, defaults to `.artifact-bot.md`.
  The file to load from the repo for use as the template.
  If this is `null`, it explicitly disables looking in the repo for templates.
- `mr_template_source` - Optional, template source for the message,
  to use if the template file cannot be loaded or is explicitly disabled.
  Typically, you will want to set this to an array of strings,
  each one representing a line.
  (That is, the entries are combined with `\n`.)
  However, you may also specify just a single string here as well.

#### Template syntax

A complete description of [Jinja2 templates](https://jinja.palletsprojects.com/en/3.0.x/templates/) is out of scope here,
so see that documentation if the information below,
plus the samples included,
don't answer your questions.

Note that the result of template processing is submitted to GitLab,
which then interpretes it as Markdown.
This may affect your formatting, and is used to format generated links, etc.

In your templates, you may use:

- property: `meta` - a summary of commit ID and pipeline ID and URL.
  Recommended to include in every message.
- property: `commit` - the commit hash used in this pipeline.
- property: `status` - the pipeline status (as defined in the GitLab API: `success` or `failed`)
- property: `pipeline_id` - the numeric ID of the pipeline.
- property: `pipeline_url` - the URL of the pipeline summary page for this pipeline.
- property: `jobs` - acts like a dictionary of job name-job result pairs: e.g. `jobs['my-job-name']`.
  Every key is valid, but if there's no job by that name,
  a mock result (with `.exists = False`) is returned.
  Job results have the following properties and methods:
  - property: `exists` - True for all job names that actually exist in this pipeline.
  - property: `has_artifacts` - True if any artifacts were uploaded from this job.
  - property: `success` - True if the job succeeded.
  - property: `web_url` - The URL of the job status page (or None for a mock result)
  - method: `make_artifact_link(text, artifactAction, path)` - make a Markdown-styled link to some aspect of a job.
    - `text` is the text of your link
    - `artifactAction` is the desired action,
      one of the globals `LOG`, `DOWNLOAD`, `BROWSE`, or `DISPLAY`,
      for job build log, direct link to artifact download, browse all job artifacts,
      or (attempt to) view an artifact inline in GitLab.
    - `path` is only required for `DOWNLOAD` and `DISPLAY`,
      and is the path (relative to the repo/artifact root) of the desired artifact.
    - If the job does not exist, or does not have any artifacts (for `DOWNLOAD`, `BROWSE`, and `DISPLAY`),
      this method still works, but renders a warning message instead of a link.

## Environment configuration

You need to provide access details for your GitLab instance
through the environment -
either manually (e.g. `export GL_URL=https://gitlab.com`),
through some service-specific configuration (like on Heroku or docker-compose),
or via a `.env` file (as often used for local testing).

The following environment variables are queried:

- `GL_URL` - the GitLab instance URL. For example, `https://gitlab.com`

- `GL_USERNAME` - The username the bot should impersonate on GitLab.
  Consider using a dedicated "bot" account if acceptable and permitted on your instance.

- `GL_ACCESS_TOKEN` - A "Personal Access Token" associated with the username configured.
  Needs the `api` scope - sadly the GitLab authorization scopes aren't very fine-grained.

- `GL_SECRET` - An arbitrary secret that must match between the webhook listener and
  your GitLab projects' integrations settings.
  You may generate one with a command like `python3 -c "import secrets; print(secrets.token_hex(16))"`.
  Only used by the webhook listener.

- `PORT` - Optional, the port for the webhook bot to listen on. Defaults to 8080.
  Only used by the webhook listener.

- `BOT_CONFIG_FILES` - Optional, one or more colon-delimited filenames or paths to config files.
  Defaults to `config.json`.

- `LOG_LEVEL` - Optional, may set to `DEBUG` to get really noisy logging output.
  Defaults to `INFO`, which shows little more than an access log for the HTTP server.

- `BOT_TRUST_HOOK_DATA` - Optional, if set, the JSON payload of hooks will be trusted more fully,
  instead of just being queried for minimal info that is elaborated upon by additional API calls.
  Defaults to off/unset for security purposes.

The file `sample.env` has additional information, and can also serve as a starting point
for creating your own `.env` file.

## Webhook configuration

If you will be running a webhook listener bot continuously,
you'll need a way for the GitLab instance to POST data to the process.
The `./run-bot.py` script runs an HTTP server (by default, on port 8080)
for this purpose.

Once you know how to reach your bot host from your GitLab instance host,
you'll want to go into each project to monitor, under Settings, Integrations,
and add a new one:

- URL: `http://yourhosthere.example.com:8080`
- Secret token: whatever you generated for `GL_SECRET` above
- Triggers: Merge request events, Pipeline Events.
  (It's harmless to check more than these two,
  but those two are the only ones that trigger any action in the bot.)

## Coding style and formatting

The Python code in this project is formatted using autopep8,
and regularly linted using pylint or a related tool.
Both these tools are integrated into the Python support in Visual Studio Code,
but may be run on their own or with a different editor and integration.

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md)
for details on our code of conduct, and the process for submitting merge requests to us.

## Versioning

This is a young project intended mainly for continuous deployment,
so no versions yet.

## Primary Authors and Contributors

- **Rylie Pavlik** - *Initial work* - @rpavlik on many platforms

## License

The code of this project
is licensed under the [Apache Software License v2.0](https://www.apache.org/licenses/LICENSE-2.0).
This does not include any projects used as dependencies or incorporated by
virtualenv, pip, Docker, Heroku, etc. - see those individual projects for specific licenses.
Reasonable efforts have been made to ensure license compatibility and broad usability,
but I am not a lawyer, so be mindful of licenses in this package, its dependencies,
and its deployment.

All files in this project shall have a copyright notice and license statement,
and should have `SPDX-License-Identifier:` tags.

## Acknowledgments

- [Collabora](https://collabora.com) for supporting Rylie's development and
  maintenance of this code in the course of her work.
- The staff of the [Khronos Group](https://khronos.org), for their patience and assistance with the
  original development and deployment of this bot.
- The developers of the libraries and tools this project uses,
  including gidgetlab, aiohttp, jinja2, python-dotenv, cachetools, and Python.

## Copyright and License for this README.md file

For this file only:

> Initially written by Rylie Pavlik. Copyright 2018-2019 Collabora, Ltd.
>
> SPDX-License-Identifier: CC-BY-4.0
