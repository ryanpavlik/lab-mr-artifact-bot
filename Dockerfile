# Copyright (c) 2019, 2021, 2024 Collabora, Ltd.
#
# SPDX-License-Identifier: Apache-2.0

FROM docker.io/library/python:3.11

WORKDIR /usr/src/app

COPY requirements.txt .

RUN pip3 install --no-cache-dir -r requirements.txt

COPY artifact_status/*.py ./artifact_status/
COPY *.py ./

# You must create this locally - don't commit it to the public repo!
# COPY config.json .

CMD [ "python3", "./run-bot.py" ]

